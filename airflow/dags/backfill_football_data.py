from datetime import datetime
from airflow import DAG
from airflow.operators.docker_operator import DockerOperator
import os

dag = DAG('backfill_football_data', description='backfill_football_data', schedule_interval='0 23 * * *', start_date=datetime(2020, 2, 28), catchup=False)

backfill_competitions_matches = DockerOperator(
    dag=dag,
    task_id='backfill_competitions_matches',
    image='yannibenoitiyeze/football-api-crawler',
    auto_remove=True,
    force_pull=True,
    docker_url='unix://var/run/docker.sock',
    command='python /home/crawler/crawl_matchs.py --dateFrom 2018-01-01 --crawler backfill',
    environment={
        "DB_DWH_USER": os.environ.get("DB_DWH_USER"),
        "DB_DWH_PASSWORD": os.environ.get("DB_DWH_PASSWORD"),
        "DB_DWH_HOSTNAME": os.environ.get("DB_DWH_HOSTNAME"),
        "DB_DWH_PORT": os.environ.get("DB_DWH_PORT"),
        "DB_DWH_DATABASE": os.environ.get("DB_DWH_DATABASE"),
        "FOOT_API_KEY": os.environ.get("FOOT_API_KEY")
        },
    network_mode='host'
)

backfill_etl_dwh = DockerOperator(
    dag=dag,
    task_id='backfill_etl_dwh',
    image='registry.gitlab.com/personal-projects16/tests-technos/airflow-and-dbt/etl:latest',
    auto_remove=True,
    force_pull=True,
    docker_url='unix://var/run/docker.sock',
    command='dbt run',
    environment={
        "DB_DWH_USER": os.environ.get("DB_DWH_USER"),
        "DB_DWH_PASSWORD": os.environ.get("DB_DWH_PASSWORD"),
        "DB_DWH_HOSTNAME": os.environ.get("DB_DWH_HOSTNAME"),
        "DB_DWH_PORT": os.environ.get("DB_DWH_PORT"),
        "DB_DWH_DATABASE": os.environ.get("DB_DWH_DATABASE")
    },
    network_mode='host'
)

backfill_competitions_matches >> backfill_etl_dwh

