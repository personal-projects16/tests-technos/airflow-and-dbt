from datetime import datetime
from airflow import DAG
from airflow.operators.docker_operator import DockerOperator
import os



dag = DAG('update_football_data', description='update_football_data', schedule_interval='0 21 * * *', start_date=datetime(2020, 2, 28), catchup=False)



update_competitions = DockerOperator(
    dag=dag,
    task_id='update_competitions',
    image='yannibenoitiyeze/football-api-crawler',
    auto_remove=True,
    docker_url='unix://var/run/docker.sock',
    command='python /home/crawler/crawl_matchs.py --crawler competitions',
    environment={
        "DB_DWH_USER": os.environ.get("DB_DWH_USER"),
        "DB_DWH_PASSWORD": os.environ.get("DB_DWH_PASSWORD"),
        "DB_DWH_HOSTNAME": os.environ.get("DB_DWH_HOSTNAME"),
        "DB_DWH_PORT": os.environ.get("DB_DWH_PORT"),
        "DB_DWH_DATABASE": os.environ.get("DB_DWH_DATABASE"),
        "FOOT_API_KEY": os.environ.get("FOOT_API_KEY")
        },
    network_mode='host'
)

update_matches = DockerOperator(
    dag=dag,
    task_id='update_matches',
    image='yannibenoitiyeze/football-api-crawler',
    auto_remove=True,
    docker_url='unix://var/run/docker.sock',
    command='python /home/crawler/crawl_matchs.py --crawler matches',
    environment={
        "DB_DWH_USER": os.environ.get("DB_DWH_USER"),
        "DB_DWH_PASSWORD": os.environ.get("DB_DWH_PASSWORD"),
        "DB_DWH_HOSTNAME": os.environ.get("DB_DWH_HOSTNAME"),
        "DB_DWH_PORT": os.environ.get("DB_DWH_PORT"),
        "DB_DWH_DATABASE": os.environ.get("DB_DWH_DATABASE"),
        "FOOT_API_KEY": os.environ.get("FOOT_API_KEY")
        },
    network_mode='host'
)



update_competitions >> update_matches

