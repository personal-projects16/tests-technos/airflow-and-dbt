certifi==2019.11.28
chardet==3.0.4
idna==2.9
peewee==3.13.1
psycopg2-binary==2.8.4
requests==2.23.0
urllib3==1.25.8
tqdm==4.32.2
