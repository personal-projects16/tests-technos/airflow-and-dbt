from crawlers.CrawlerMatch import CrawlerMatch
from crawlers.CrawlerCompetition import CrawlerCompetition
from db import db
from models.matchs import Match, MatchLineup, MatchGoals, MatchBookings, MatchSubstitutions, MatchReferees
from models.competitions import Competition
import argparse
from datetime import date, datetime
from tqdm import tqdm

def main(dateFrom, dateTo, status, crawler):
  db.connect()
  db.execute_sql('CREATE SCHEMA IF NOT EXISTS stg;')
  db.create_tables([
      Match, 
      MatchLineup,
      MatchGoals,
      MatchBookings,
      MatchSubstitutions,
      MatchReferees,
      Competition
  ])


  params = {
    "dateFrom": dateFrom,
    "dateTo": dateTo,
    "status": status
  } 

  

  if crawler == "backfill":

    crawler_dimensions = CrawlerCompetition()
    crawler_dimensions.saveCompetitions()

    query = Competition.select(Competition.id)
    competitions = [c["id"] for c in list(query.dicts())]
    if competitions:
      for competition_id in tqdm(competitions) :

        params['competitions'] = competition_id
        try:
          crawler_matches = CrawlerMatch(params=params, competition_id=competition_id)
          crawler_matches.saveMatches()
          crawler_matches.saveMatchesLineup()
          crawler_matches.saveMatchesGoals()
          crawler_matches.saveMatchesBookings()
          crawler_matches.saveMatchesSubstitutions()
          crawler_matches.saveMatchesReferees()
        # some competitions are unreachable so scripts must continue
        except:
          continue
    else:
      raise ValueError("Error when crawling matches, competitions not found")
  
  elif crawler == "matches":
    if abs((datetime.strptime(dateFrom, '%Y-%m-%d').date() - datetime.strptime(dateTo, '%Y-%m-%d').date()).days) <= 10:
      crawler_matches = CrawlerMatch(params=params)
      crawler_matches.saveMatches()
      crawler_matches.saveMatchesLineup()
      crawler_matches.saveMatchesGoals()
      crawler_matches.saveMatchesBookings()
      crawler_matches.saveMatchesSubstitutions()
      crawler_matches.saveMatchesReferees()
    
    else:
      raise ValueError("You can only request lest than 10 days")
  
  elif crawler == "competitions":
    crawler_dimensions = CrawlerCompetition()
    crawler_dimensions.saveCompetitions()
    

if __name__ == "__main__":
    # execute only if run as a script
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--dateFrom", default=str(date.today()),
                        help="Filter Start Date (YYYY-mm-dd)")
    parser.add_argument("-e", "--dateTo", default=str(date.today()),
                        help="Filter End Date (YYYY-mm-dd)")
    parser.add_argument("-f", "--status", default="FINISHED",
                        help="Match Status")
    parser.add_argument("-c", "--crawler", default="matches",
                        help="Crawler name (competitions, matches or backfill (both)")
    args = parser.parse_args()

    main(args.dateFrom, args.dateTo, args.status, args.crawler)