from peewee import *
from db import db

class BaseModel(Model):
      class Meta:
        database = db
        schema = 'stg'

class Match(BaseModel):
  competition_id = IntegerField(null=True)
  id = IntegerField(primary_key=True)
  season__id = IntegerField()
  season__start_date = CharField()
  season__end_date = CharField()
  season__current_matchday = IntegerField()
  utc_date = CharField()
  status = CharField()
  attendance = IntegerField(null=True)
  matchday = IntegerField(null=True)
  stage = CharField(null=True)
  group = CharField(null=True)
  last_updated = CharField()
  home_team__id = IntegerField(null=True)
  home_team__name = CharField(null=True)
  home_team__coach__id = IntegerField(null=True)
  home_team__captain__id = IntegerField(null=True)
  home_team__captain__shirt_number = IntegerField(null=True)
  away_team__id = IntegerField(null=True)
  away_team__name = CharField(null=True)
  away_team__coach__id = IntegerField(null=True)
  away_team__captain__id = IntegerField(null=True)
  away_team__captain__shirt_number = IntegerField(null=True)
  score__winner = CharField(null=True)
  score__duration = CharField(null=True)
  score__full_time__home_team = IntegerField(null=True)
  score__full_time__away_team = IntegerField(null=True)
  score__half_time__home_team = IntegerField(null=True)
  score__half_time__away_team = IntegerField(null=True)
  score__extra_time__home_team = IntegerField(null=True)
  score__extra_time__away_team = IntegerField(null=True)
  score__penalties__home_team = IntegerField(null=True)
  score__penalties__away_team = IntegerField(null=True)
  class Meta:
    db_table = 'matches'

class MatchLineup(BaseModel):
  match_id = IntegerField()
  team = CharField()
  player_id = IntegerField()
  player_name = CharField()
  player_position = CharField()
  player_shirt_number = IntegerField()
  is_substitute = BooleanField()
  class Meta:
    db_table = 'matches_lineup'

class MatchGoals(BaseModel):
  match_id = IntegerField()
  minute = IntegerField()
  goal_type = CharField()
  team__id = IntegerField()
  team__name = CharField()
  scorer__id = IntegerField()
  scorer__name = CharField()
  assist__id = IntegerField()
  assist__name = CharField()
  class Meta:
    db_table = 'matches_goals'
  
class MatchBookings(BaseModel):
  match_id = IntegerField()
  minute = IntegerField()
  team__id = IntegerField()
  team__name = CharField()
  player__id = IntegerField()
  player__name = CharField()
  card = CharField()
  class Meta:
    db_table = 'matches_bookings'

class MatchSubstitutions(BaseModel):
  match_id = IntegerField()
  minute = IntegerField()
  team__id = IntegerField()
  team__name = CharField()
  player_in__id = IntegerField()
  player_in__name = CharField()
  player_out__id = IntegerField()
  player_out__name = CharField()
  class Meta:
    db_table = 'matches_substitutions'

class MatchReferees(BaseModel):
  match_id = IntegerField()
  referee_id = IntegerField()
  name = CharField(null=True)
  nationality = CharField(null=True)
  class Meta:
    db_table = 'matches_referees'
