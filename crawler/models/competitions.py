from peewee import *
from db import db

class BaseModel(Model):
      class Meta:
        database = db
        schema = 'stg'

class Competition(BaseModel):
  id = IntegerField(primary_key=True)
  area__id = IntegerField(null=True)
  area__name = CharField(null=True)
  area__countryCode = CharField(null=True)
  area__ensignUrl = CharField(null=True)
  name = CharField(null=True)
  code = CharField(null=True)
  emblemUrl = CharField(null=True)
  plan = CharField(null=True)
  current_season__id = IntegerField(null=True)
  current_season__startDate = CharField(null=True)
  current_season__endDate = CharField(null=True)
  current_season__currentMatchday= CharField(null=True)
  numberOfAvailableSeasons = IntegerField(null=True)
  lastUpdated = CharField(null=True)

  class Meta:
    db_table = 'competitions'