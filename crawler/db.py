import os
from peewee import PostgresqlDatabase
# Connect to a Postgres database.
db = PostgresqlDatabase(os.environ.get('DB_DWH_DATABASE'), user=os.environ.get('DB_DWH_USER'), password=os.environ.get('DB_DWH_USER'),
                           host=os.environ.get('DB_DWH_HOSTNAME'), port=os.environ.get('DB_DWH_PORT'))  
