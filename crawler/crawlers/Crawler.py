import requests
import os
import json
import time
import logging

class Crawler:
  def __init__(self):
    
    if not os.environ.get("FOOT_API_KEY"):
      raise Exception("You should provide an access token")
    
    self.headers = { 'X-Auth-Token': os.environ.get("FOOT_API_KEY") }
    self.baseUrl = "http://api.football-data.org/v2"


  def get(self, url, params):
    res = requests.get(f'{self.baseUrl}{url}', headers = self.headers, params = params)
    try:
      remainingRequests = int(res.headers['X-Requests-Available-Minute'])
      timeResetRequests = int(res.headers['X-RequestCounter-Reset'])
      
      # Sleep to avoid rate limit
      if remainingRequests <= 2:
        time.sleep(timeResetRequests + 5)
        raise Warning(f"SLEEP MODE {remainingRequests} left ,counter will be reset in {timeResetRequests} seconds ")

      result = {
        "remainingRequests": remainingRequests,
        "timeResetRequests": timeResetRequests,
        "result": json.loads(res.text)
      }
    except Exception as e:
      result = dict()
      logging.exception(f"Error when connecting to {url}: {e}")

    return result