import requests
import os
from crawlers.Crawler import Crawler
from models.competitions import Competition
from db import db

class CrawlerCompetition(Crawler):
  def __init__(self, params={}):
    Crawler.__init__(self)
    self.url = "/competitions"
    self.competitions = self.get(self.url, params)
  

  def saveCompetitions(self):
    mapped_competitions = []
    try:
      for competition in self.competitions.get("result", {}).get("competitions"):
          m = {
              "id": competition.get("id"),
              "area__id": competition.get("area", {}).get("id"),
              "area__name": competition.get("area", {}).get("name"),
              "area__countryCode": competition.get("area", {}).get("countryCode"),
              "area__ensignUrl": competition.get("area", {}).get("ensignUrl"),
              "name": competition.get("name"),
              "code": competition.get("code"),
              "emblemUrl": competition.get("emblemUrl"),
              "plan": competition.get("plan"),
              "lastUpdated": competition.get("lastUpdated"),
          }
          if isinstance(competition.get("currentSeason"), dict):
              m["current_season__id"]= competition.get("currentSeason", {}).get("id")
              m["current_season__startDate"]= competition.get("currentSeason", {}).get("startDate")
              m["current_season__endDate"]= competition.get("currentSeason", {}).get("endDate")
              m["current_season__currentMatchday"] = competition.get("currentSeason", {}).get("currentMatchday")
          else :
              m["current_season__id"]= -1
              m["current_season__startDate"]= ""
              m["current_season__endDate"]= ""
              m["current_season__currentMatchday"] = ""
          
          mapped_competitions.append(m)
      with db.atomic():
        Competition.insert_many(mapped_competitions).on_conflict(action='IGNORE').execute()
    
    except requests.exceptions.RequestException as e:     
      raise ConnectionError(f"Error during requesting data for {self.url}: {e}")
    
    except Exception as e:     
      raise ConnectionError(f"Error for {self.url}: {e}")