import requests
import os
from crawlers.Crawler import Crawler
from models.matchs import Match, MatchLineup, MatchGoals, MatchBookings, MatchSubstitutions, MatchReferees
from db import db
import logging 

class CrawlerMatch(Crawler):
      
  def __init__(self, params={},competition_id=None):
    Crawler.__init__(self)
    self.competition_id = competition_id
    if competition_id:
      self.url = f"/competitions/{competition_id}/matches"
    else:
      self.url = "/matches"
    
    self.matches = self.get(self.url, params)



  def saveMatches(self):
    mapped_matches = []
          
    try:
      for match in self.matches.get("result", {}).get("matches"):
        if self.competition_id:
              competition_id = self.competition_id
        else:
              competition_id = match.get("competition", {}).get("id")
    
        m = {
          "competition_id": competition_id,
          "id": match.get("id"),
          "season__id": match.get("season", {}).get("id"),
          "season__start_date": match.get("season", {}).get("startDate"),
          "season__end_date": match.get("season", {}).get("endDate"),
          "season__current_matchday": match.get("season", {}).get("currentMatchday"),
          "utc_date": match.get("utcDate"),
          "status": match.get("status"),
          "attendance": match.get("attendance"),
          "matchday": match.get("matchday"),
          "stage": match.get("stage"),
          "group": match.get("group"),
          "last_updated": match.get("lastUpdated"),
          "home_team__id": match.get("homeTeam", {}).get("id"),
          "home_team__name": match.get("homeTeam", {}).get("name"),
          "home_team__coach__id": match.get("homeTeam", {}).get("coach", {}).get("id"),
          "home_team__captain__id": match.get("homeTeam", {}).get("captain", {}).get("id"),
          "home_team__captain__shirt_number": match.get("homeTeam", {}).get("captain", {}).get("shirtNumber"),
          "away_team__id": match.get("awayTeam", {}).get("id"),
          "away_team__name": match.get("awayTeam", {}).get("name"),
          "away_team__coach__id": match.get("awayTeam", {}).get("coach", {}).get("id"),
          "away_team__captain__id": match.get("awayTeam", {}).get("captain", {}).get("id"),
          "away_team__captain__shirt_number": match.get("awayTeam", {}).get("captain", {}).get("shirtNumber"),
          "score__winner": match.get("score", {}).get("winner"),
          "score__duration": match.get("score", {}).get("duration"),
          "score__full_time__home_team": match.get("score", {}).get("fullTime", {}).get("homeTeam"),
          "score__full_time__away_team": match.get("score", {}).get("fullTime", {}).get("awayTeam"),
          "score__half_time__home_team": match.get("score", {}).get("halfTime", {}).get("homeTeam"),
          "score__half_time__away_team": match.get("score", {}).get("halfTime", {}).get("awayTeam"),
          "score__extra_time__home_team": match.get("score", {}).get("extraTime", {}).get("homeTeam"),
          "score__extra_time__away_team": match.get("score", {}).get("extraTime", {}).get("awayTeam"),
          "score__penalties__home_team": match.get("score", {}).get("penalties", {}).get("homeTeam"),
          "score__penalties__away_team": match.get("score", {}).get("penalties", {}).get("awayTeam")
        }
        mapped_matches.append(m)
      with db.atomic():
        Match.insert_many(mapped_matches).on_conflict(action='IGNORE').execute()
    
    except requests.exceptions.RequestException as e:     
      raise ConnectionError(f"Error during requesting data for {self.url}: {e}")
    
    except Exception as e:     
      logging.exception(f"Error when connecting to {self.url}: {e}")


  def saveMatchesLineup(self):
    mapped_matches_lineup = []
    for match in self.matches.get("result", {}).get("matches"):
      for player in match.get("homeTeam", {}).get("lineup", {}):
        m = {
          "match_id": match.get("id"),
          "team": "HOME_TEAM",
          "player_id": player.get("id"),
          "player_name": player.get("name"),
          "player_position": player.get("position"),
          "player_shirt_number": player.get("shirtNumber"),
          "is_substitute": False
        }
        mapped_matches_lineup.append(m)
      
      for player in match.get("homeTeam", {}).get("bench", {}):
        m = {
          "match_id": match.get("id"),
          "team": "HOME_TEAM",
          "player_id": player.get("id"),
          "player_name": player.get("name"),
          "player_position": player.get("position"),
          "player_shirt_number": player.get("shirtNumber"),
          "is_substitute": True
        }
        mapped_matches_lineup.append(m)

      for player in match.get("awayTeam", {}).get("lineup", {}):
        m = {
          "match_id": match.get("id"),
          "team": "AWAY_TEAM",
          "player_id": player.get("id"),
          "player_name": player.get("name"),
          "player_position": player.get("position"),
          "player_shirt_number": player.get("shirtNumber"),
          "is_substitute": False
        }
        mapped_matches_lineup.append(m)
      
      for player in match.get("awayTeam", {}).get("bench", {}):
        m = {
          "match_id": match.get("id"),
          "team": "AWAY_TEAM",
          "player_id": player.get("id"),
          "player_name": player.get("name"),
          "player_position": player.get("position"),
          "player_shirt_number": player.get("shirtNumber"),
          "is_substitute": True
        }
        mapped_matches_lineup.append(m)
    with db.atomic():
      MatchLineup.insert_many(mapped_matches_lineup).on_conflict(action='IGNORE').execute()


  def saveMatchesGoals(self):
    mapped_matches_goals = []
    for match in self.matches.get("result", {}).get("matches"):
      for goal in match.get("goals", {}):
        m = {
            "match_id": match.get("id"),
            "minute": goal.get("minute"),
            "goal_type": goal.get("goal_type"),
            "team__id": goal.get("team", {}).get("id"),
            "team__name": goal.get("team", {}).get("name"),
            "scorer__id": goal.get("scorer", {}).get("id"),
            "scorer__name": goal.get("scorer", {}).get("name"),
            "assist__id": goal.get("assist", {}).get("id"),
            "assist__name": goal.get("assist", {}).get("name")
        }
        mapped_matches_goals.append(m)
      with db.atomic():
        MatchGoals.insert_many(mapped_matches_goals).on_conflict(action='IGNORE').execute()

  def saveMatchesBookings(self):
    mapped_matches_bookings = []
    for match in self.matches.get("result", {}).get("matches"):
      for booking in match.get("bookings", {}):
        m = {
            "match_id": match.get("id"),
            "minute": goal.get("minute"),
            "team__id": goal.get("team", {}).get("id"),
            "team__name": goal.get("team", {}).get("name"),
            "player__id": goal.get("player", {}).get("id"),
            "player__name": goal.get("player", {}).get("name"),
            "card": goal.get("card", {})
        }
        mapped_matches_bookings.append(m)
      with db.atomic():
        MatchBookings.insert_many(mapped_matches_bookings).on_conflict(action='IGNORE').execute()

  def saveMatchesSubstitutions(self):
    mapped_matches_bookings = []
    for match in self.matches.get("result", {}).get("matches"):
      for booking in match.get("substitions", {}):
        m = {
            "match_id": match.get("id"),
            "minute": goal.get("minute"),
            "team__id": goal.get("team", {}).get("id"),
            "team__name": goal.get("team", {}).get("name"),
            "player_in__id": goal.get("playerIn", {}).get("id"),
            "player_in__name": goal.get("playerIn", {}).get("name"),
            "player_out__id": goal.get("playerOut", {}).get("id"),
            "player_out__name": goal.get("playerOut", {}).get("name"),
        }
        mapped_matches_bookings.append(m)
      with db.atomic():
        MatchSubstitutions.insert_many(mapped_matches_bookings).on_conflict(action='IGNORE').execute()

  def saveMatchesReferees(self):
    mapped_matches_referees = []
    for match in self.matches.get("result", {}).get("matches"):
      for referee in match.get("referees", {}):
        m = {
              "match_id": match.get("id"),
              "referee_id": referee.get("id"),
              "name": referee.get("name"),
              "nationality": referee.get("nationality")
        }
        mapped_matches_referees.append(m)
      with db.atomic():
        MatchReferees.insert_many(mapped_matches_referees).on_conflict(action='IGNORE').execute()
