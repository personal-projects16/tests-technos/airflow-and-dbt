{{ config(materialized='table') }}

with areas as (

    select distinct *,
    now() as created_at
    from {{ ref('raw_areas') }}
)

select *
from areas