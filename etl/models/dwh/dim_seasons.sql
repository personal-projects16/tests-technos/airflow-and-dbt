{{ config(materialized='table') }}

with raw_seasons as (

    select distinct *,
    now() as created_at
    from {{ ref('raw_seasons') }}
)

select *
from raw_seasons