{{ config(materialized='table') }}

with raw_competitions as (

    select *,
    now() as created_at
    from {{ ref('raw_competitions') }}
)

select *
from raw_competitions