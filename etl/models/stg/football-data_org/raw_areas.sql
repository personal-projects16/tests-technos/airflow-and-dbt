{{ config(materialized='ephemeral') }}
{{ config(tags=["competitions", "hebdo"]) }}

with source_data as (

    select area__id as area_id,
    area__name as area_name,
    "area__countryCode" as area_country_code,
    "area__ensignUrl" as area_ensign_url
    from {{ source('competitions_api', 'competitions') }}
)

select *
from source_data