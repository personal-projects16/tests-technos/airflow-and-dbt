{{ config(materialized='ephemeral') }}
{{ config(tags=["competitions", "hourly"]) }}

with seasons as (

    select "current_season__id" as season_id,
    "current_season__startDate" as season_start_date,
    "current_season__endDate" as season_end_date,
    "current_season__currentMatchday" as season_current_matchday
    from {{ source('competitions_api', 'competitions') }}

)

select *
from seasons