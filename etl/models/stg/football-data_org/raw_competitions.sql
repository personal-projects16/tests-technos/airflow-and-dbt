{{ config(materialized='ephemeral') }}
{{ config(tags=["competitions", "yearly"]) }}


with competitions as (

    select id as comp_id,
    area__id as area_id,
    current_season__id as current_season_id,
    name as comp_name,
    code as comp_code,
    "emblemUrl" as comp_emblem_url,
    "plan" as comp_plan
    from {{ source('competitions_api', 'competitions') }}
)

select *
from competitions