.DEFAULT_GOAL := help

CONTAINER_PREFIX=annotation
DC=docker-compose -p ${CONTAINER_PREFIX}

copy-env: ## Create the .env file at the root
	cp -n .env.template .env;  if [ $$? -eq 0 ] ; then echo "\n\033[31m[ERR] File .env already exists\033[0m\n" ; fi

setup: pull build up ## Setup the development environment

pull: ## Pull the external images
	${DC} pull

build: ## Build the containers and pull FROM statements
	${DC} build

rebuild: ## Rebuild containers
	${MAKE} down build up

up: ## Up the containers
	${DC} up -d

down: ## Down the containers (keep volumes)
	${DC} down

destroy: ## Destroy the containers, volumes, networks…
	${DC} down -v --remove-orphan

start: ## Start the containers
	${DC} start

stop: ## Stop the containers
	${DC} stop

prod-deploy: ## Deploy the containers dor production
	${DC} -f docker-compose.yml -f docker-compose.prod.yml down -v --remove-orphan
	${DC} -f docker-compose.yml -f docker-compose.prod.yml build --pull --no-cache
	${DC} -f docker-compose.yml -f docker-compose.prod.yml up -d

restart: ## Restart the containers
	${MAKE} down up

bash: ARGS = front
bash: ## Run bash shell
	${DC} exec ${ARGS} bash

.PHONY: logs
logs: ## Show containers logs
	${DC} logs -f --tail="100"

dc: ARGS = ps
dc: ## Run docker-compose command. Use ARGS="" to pass parameters to docker-compose.
	${DC} ${ARGS}

backup: ## Backup dist directory in backups folder
	chmod +x data_backup.sh
	./data_backup.sh

restore: ARGS = '2019-03-18'
restore: ## Restore dist directory from backups folder
	${MAKE} down
	chmod +x data_restore.sh
	./data_restore.sh ${ARGS}
	${MAKE} up

### OTHERS
# ¯¯¯¯¯¯¯¯

.PHONY: help
help: ## Display this help
	@IFS=$$'\n'; for line in `grep -E '^[a-zA-Z_#-]+:?.*?## .*$$' $(MAKEFILE_LIST)`; do if [ "$${line:0:2}" = "##" ]; then \
	echo $$line | awk 'BEGIN {FS = "## "}; {printf "\n\033[33m%s\033[0m\n", $$2}'; else \
	echo $$line | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-25s\033[0m %s\n", $$1, $$2}'; fi; \
	done; unset IFS;
